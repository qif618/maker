package ldh.maker.freemaker;

import ldh.database.Table;

/**
 * Created by ldh on 2017/4/16.
 */
public class JavafxSearchControllerMaker extends BeanMaker<JavafxSearchControllerMaker> {

    protected Table table;
    protected String projectPackage;
    protected String pojoPackage;

    public JavafxSearchControllerMaker table(Table table) {
        this.table = table;
        return this;
    }

    public JavafxSearchControllerMaker projectPackage(String projectPackage) {
        this.projectPackage = projectPackage;
        return this;
    }

    public JavafxSearchControllerMaker pojoPackage(String pojoPackage) {
        this.pojoPackage = pojoPackage;
        return this;
    }

    @Override
    public JavafxSearchControllerMaker make() {
        data();
        out(ftl, data);

        return this;
    }

    @Override
    public void data() {
        fileName = table.getJavaName() + "SearchController.java";
        data.put("table", table);
        data.put("projectPackage", projectPackage);
        data.put("pojoPackage", pojoPackage);
        data.put("controllerPackage", pack);
    }
}
