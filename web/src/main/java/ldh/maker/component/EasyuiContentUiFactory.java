package ldh.maker.component;

/**
 * Created by ldh on 2017/4/6.
 */
public class EasyuiContentUiFactory extends ContentUiFactory {

    public ContentUi create() {
        return new EasyuiContentUi();
    }
}
