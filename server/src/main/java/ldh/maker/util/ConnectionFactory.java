package ldh.maker.util;

import javafx.beans.property.StringProperty;
import ldh.maker.vo.DBConnectionData;

import java.sql.*;

/**
 * Created by ldh on 2017/2/26.
 */
public class ConnectionFactory {

    public static Connection getConnection(DBConnectionData data) {
        String url = "jdbc:mysql://" + data.getIpProperty() + ":" + data.getPortProperty() + "?serverTimezone=UTC";
        try{
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection con = DriverManager.getConnection(url, data.getUserNameProperty(), data.getPasswordProperty()) ;
            return con;
        }catch(Exception se){
            se.printStackTrace() ;
        }
        return null;
    }

    public static Connection getConnection(DBConnectionData data, String db) {
        String url = "jdbc:mysql://" + data.getIpProperty() + ":" + data.getPortProperty() + "/" + db + "?serverTimezone=UTC";
        try{
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection con = DriverManager.getConnection(url, data.getUserNameProperty(), data.getPasswordProperty()) ;
            return con;
        }catch(Exception se){
            se.printStackTrace() ;
        }
        return null;
    }

    public  static void close(Connection connection)  {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public  static void close(Statement statement)  {
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
