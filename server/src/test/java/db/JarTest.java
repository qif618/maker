package db;

import ldh.maker.util.CopyDirUtil;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

/**
 * Created by ldh on 2017/4/13.
 */
public class JarTest {

    public static void main (String args[]) throws IOException {
        String jar = "D:\\git\\maker\\web\\target\\web-1.0.jar";
        JarFile jarFile = new JarFile(jar);
        Enumeration<JarEntry> enums = jarFile.entries();
        String pack = "common";
        while (enums.hasMoreElements()) {
            JarEntry entry = enums.nextElement();
            if (entry.getName().startsWith(pack)) {
                process2(entry);
            }
        }
//        JarEntry entry = jarFile.getJarEntry("org");
//        if (entry.isDirectory()) {
//
//        }
//        InputStream input = jarFile.getInputStream(entry);
//        process(input);
//        process2(entry);

        String str = "srcDir:file:/E:/freemaker/web-1.0.jar!/common";
        String pack1 = str.substring(str.indexOf("!"));
        str = str.substring(0, str.indexOf("jar"));
        str = str.substring(str.lastIndexOf("/")+1);

        String srcFile = "common/css/admin.css";
        String srcDir = srcFile.substring(0, srcFile.lastIndexOf("/"));
        String td = CopyDirUtil.makePath("c:", srcDir);

        String jars = str + "jar";

        String s = "com.dd.dao";
        String dd =  s.substring(0, s.lastIndexOf("."));;
        System.out.println(dd);
    }

    private static void process(InputStream input)
            throws IOException {
        InputStreamReader isr =
                new InputStreamReader(input);
        BufferedReader reader = new BufferedReader(isr);
        String line;
        while ((line = reader.readLine()) != null) {
            System.out.println(line);
        }
        reader.close();
    }

    private static void process2(JarEntry entry)
            throws IOException {
        String name = entry.getName();
        long size = entry.getSize();
        long compressedSize = entry.getCompressedSize();
        System.out.println(name + "\t" + size + "\t" + compressedSize);
    }

    @Test
    public void jarDirs() {
        String folderPath = "test/maker/web-1.0.jar!/common/bootstrap/css";
        int index = folderPath.indexOf("jar");
        String str = folderPath.substring(0, index);
        str = str.substring(str.lastIndexOf("/")+1);
        String jar = str + "jar";
        String pack = folderPath.substring(folderPath.indexOf("!")+2);
        System.out.println("jar:" + jar);
        System.out.println("jar:" + folderPath.substring(folderPath.indexOf("test/maker") + "test/maker".length()));
    }

    @Test
    public void jarFile() throws Exception {
        String jar = "E:\\test\\maker\\maker-desktop-1.0.jar";
        JarFile jarFile = new JarFile(jar);
        JarEntry jarEntry = jarFile.getJarEntry("images");

        InputStream in = jarFile.getInputStream(jarEntry);
//        Files.copy(in, Paths.get("e:\\test\\"), REPLACE_EXISTING);
    }
}
